# This is Auto Generated file! Don't Need to Modify Manually.

module "{ENVIRONMENT}" {
  source                  = "./module/aws-ec2"
  aws_ami                 = "${data.aws_ami.os.image_id}"
  instance_type           = "${var.instance_type}"
  common_sg               = "${aws_security_group.default.id}"
  key_pair                = "${aws_key_pair.deployer.id}"
  subnet_id               = "${data.aws_subnet.default.id}"
  project                 = "${var.project}"
  owner                   = "${var.env_owner}"
  creator                 = "${var.creator}"
  requester               = "${var.requester}"
  volume_size             = "${var.volume_size}"
  environment             = "{ENVIRONMENT}"
  email                   = "${var.email}"
  project_name            = "${var.project_name}"
  private_key             = "${var.private_key}"
  remote_script           = "${var.remote_script}"
  ENVIRONMENT_DESCRIPTION = "${var.ENVIRONMENT_DESCRIPTION}"
}

output "{ENVIRONMENT}_PUB_IP" {
  value = "${module.{ENVIRONMENT}.INSTANCE_PUB_IP}"
}
