resource "aws_eip" "miniweb" {
  instance = "${aws_instance.miniweb.id}"
  vpc      = true
}
