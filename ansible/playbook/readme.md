## How To Use These Ansible Playbooks?
 These ansible playbooks are dependant on each other so it will will not work standalone. these playbook will work with MiTE AWSPlatform Tools.

 if you want to execute these playbook separately then you have to make lot of modification accourding to your requirement.

 #### Important Notes:
 * Before running these playbook make sure ubuntu or debian os must be install on remote machine.
 * it will not work on Centos or rhel or other non debian distro.
 * We have tested all code on ubuntu 16.04. so you are recommended to use ubuntu 16.04 or latar verion.

 #### Step To Execute these playbook with MiTE:
  - First create your environment with this tool by using `MiTE -e <environment_name> --plan` and `MiTE -e <environment_name> --apply`
  - once environment has been created successfully then you have to execute test.yml with this command `MiTE -e <environment_name> --ansible --playbook test.yml`
  - if test.yml has been executed successfully then you can run your own playbooks one by one.
  - run this command to execute own playbook `MiTE -e <environment_name> --ansible --playbook <your_playbook.yml>`


#### Credits
These playbooks you can use free of cost in commercial or non commercial project<br>

**feel free to contact repository author/developer** </br>

|Name|details|
|--|--|
| **Author**  | Harry  |
|**Email**   | HarrytheDevOpsGuy@gmail.com  |
| **Skype**  | HarryTheITExpert  |
|**My Blog**   | www.Harry-TheDevopsGuy.blogspot.com   |

<!-- **Author**  : Harry </br>
**Email**    : HarrytheDevOpsGuy@gmail.com </br>
**Skype**    : HarryTheITExpert </br>
**blog**     : www.Harry-TheDevopsGuy.blogspot.com </br> -->

I will be very happy to reply/resolve your query asap.
