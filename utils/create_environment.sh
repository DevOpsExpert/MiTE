#!/bin/bash

check_environment(){
  export  ENV_NAME=$(echo ${1} | tr '[:upper:]' '[:lower:]')

  if [[ -z ${ENV_NAME} ]]; then
    read -e -p "Please enter Environment Name : " ENV_NAME_IN
    export  ENV_NAME=$(echo ${ENV_NAME_IN} | tr '[:upper:]' '[:lower:]')
    if [[ -z ${ENV_NAME} ]]; then
      echo "Environment Name Must Required."
      exit 0
    else
      ENV_DIR="${PROJECT_ROOT}/configs/${ENV_NAME}"
    fi
  else
    ENV_DIR="${PROJECT_ROOT}/configs/${ENV_NAME}"
  fi

  if [[ -d ${ENV_DIR} ]]; then
    echo "Environment already Exists: ${ENV_NAME}"
    echo "Please try with other name!!!"
    exit 1
  else
      echo "Creating Environment : ${ENV_DIR}"
  fi
}

create_deploy_keys() {
  echo "running ${FUNCNAME}"
  ssh-keygen -f "${ENV_DIR}/${ENV_NAME}_deploy_key" -b 2048 -t rsa -N '' -C "${Email_ID}"
  chmod 600 "${ENV_DIR}/${ENV_NAME}_deploy_key"
  chmod 600 "${ENV_DIR}/${ENV_NAME}_deploy_key.pub"
}

newpw() {
  local chars=${1}
  < /dev/urandom tr -dc _*A-Za-z0-9 | head -c"${chars}";echo;
}

write_config(){
  echo "running ${FUNCNAME}"
  local RDS_PASSWORD=$(newpw 30)
  AWS_ACCOUNT_OWNER=$(echo ${AWS_ACCOUNT_OWNER}|tr '[:lower:]' '[:upper:]')
  sed -e 's|{ENV_NAME}|'"${ENV_NAME}"'|g' \
    -e 's|{RDS_PASSWORD}|'"${RDS_PASSWORD}"'|g' \
    -e 's|{NEW_PROJECT_NAME}|'"${NEW_PROJECT_NAME}"'|g' \
    -e 's|{AWS_SERVICES}|'"${AWS_SERVICES}"'|g' \
    -e 's|{AWS_ACCOUNT_OWNER}|'"${AWS_ACCOUNT_OWNER}"'|g' \
    -e 's|{YOUR_INFRA_REGION}|'"${YOUR_INFRA_REGION}"'|g' \
    -e 's|{REQUIRED_PACKAGES}|'"${REQUIRED_PACKAGES}"'|g' \
    -e 's|{ENVIRONMENT_SETUP}|'"${ENVIRONMENT_SETUP}"'|g' \
    -e 's|{INSTANCE_TYPE}|'"${INSTANCE_TYPE}"'|g' \
    -e 's|{VOLUME_SIZE}|'"${VOLUME_SIZE}"'|g' \
    -e 's|{OFFICIAL_EMAIL}|'"${OFFICIAL_EMAIL}"'|g' \
    -e 's|{WEB_SERVER_ENVIRONMENT}|'"${WEB_SERVER_ENVIRONMENT}"'|g' \
    -e 's|{RDS_INSTANCE_TYPE}|'"${RDS_INSTANCE_TYPE}"'|g' \
    -e 's|{RDS_DB_VOLUME_SIZE}|'"${RDS_DB_VOLUME_SIZE}"'|g' \
    -e 's|{RDS_DB_ENGINE}|'"${RDS_DB_ENGINE}"'|g' \
    -e 's|{RDS_DB_ENGINE_VERSION}|'"${RDS_DB_ENGINE_VERSION}"'|g' \
    -e 's|{RDS_DB_USERS}|'"${RDS_DB_USERS}"'|g' \
    -e 's|{RDS_DB_NAMES}|'"${RDS_DB_NAMES}"'|g' \
    -e 's|{RDS_DB_PORT}|'"${RDS_DB_PORT}"'|g' \
    -e 's|{S3_BUCKETS_NAME}|'"${S3_BUCKETS_NAME}"'|g' \
    -e 's|{ENVIRONMENT_REQUESTED_BY}|'"${ENVIRONMENT_REQUESTED_BY}"'|g' \
    -e 's|{ENVIRONMENT_CREATED_BY}|'"${ENVIRONMENT_CREATED_BY}"'|g' \
    -e 's|{ENVIRONMENT_TICKET_ID}|'"${ENVIRONMENT_TICKET_ID}"'|g' \
    --in-place=.bak "${CONFIG}"
  rm "${CONFIG}.bak"
}

update_domain_name(){
ENVIRONMENTS=(${ENVIRONMENT_SETUP})
DOMAINS=(${PROJECT_DOMAIN_NAME})
config_domain(){
  for ((i=0;i<${1};++i)); do
    #echo "[\"${ENVIRONMENTS[$i]}_domain_name\"]=\"${DOMAINS[$i]}\""
    sed -i '/ANSIBLE_ENV_DEPENDANT_VARS/a '"[\"${ENVIRONMENTS[$i]}_domain_name\"]=\"${DOMAINS[$i]}\"" ${CONFIG}
done
}

if [[ "${#DOMAINS[@]}"<="${#ENVIRONMENTS[@]}" ]]; then
config_domain ${#DOMAINS[@]}
else
config_domain ${#ENVIRONMENTS[@]}
fi
}



create_config() {
  cp "${CONFIG_TEMPLATE}" "${CONFIG}"
  write_config
  update_domain_name
}

create_environment(){
if [[ ! -d ${ENV_DIR} ]]; then
  mkdir "${ENV_DIR}"
  echo
  echo "This is the config directory for  \"${ENV_NAME}\"" > "${ENV_DIR}/README.md"

  CONFIG_TEMPLATE="${PROJECT_ROOT}/templates/environment_config.example"
  CONFIG="${ENV_DIR}/${ENV_NAME}.config"

  create_deploy_keys
  create_config
fi
}

create_new_branch(){
  cd ${PROJECT_ROOT}
  REPO_CLEAN=$(git status|awk 'END {print $NF}')
  GIT_BRANCH_NAME="ENVIRONMENT/${ENV_NAME}"
  BRANCH_CHECK="$(git branch -a |grep 'remote'| grep  ${GIT_BRANCH_NAME}|cut -d '/' -f1)"

  if [[ "${REPO_CLEAN}" == "clean" && "${BRANCH_CHECK}" != "remotes" ]]; then
    local_branch="$(git branch -a|grep ${GIT_BRANCH_NAME})"
    if [[ ! -z ${local_branch} ]]; then
      # git stash -u
      # git checkout fix/bugs
      echo "branch already exist"
      if [[ $USER != "jenkins" ]]; then
        git branch -D ${GIT_BRANCH_NAME}
      fi
    fi

    if [[ $USER != "jenkins" ]]; then
      git checkout -b ${GIT_BRANCH_NAME}
    fi

    create_environment
  else
    if [[ ! -z ${BRANCH_CHECK} ]]; then
      echo "${ENV_NAME} : already in use!! Try with another environment name."
    else
      echo "Please Discard your changes."
    fi

  fi

}

PROJECT_ROOT="$(git rev-parse --show-toplevel)"
check_environment ${1}
echo
echo "You entered ${ENV_NAME}"
echo
if [[ $USER == "jenkins" ]]; then
  create_new_branch
else
  create_environment
fi
