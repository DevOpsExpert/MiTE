#!/usr/bin/env bash

# get_aws_regions(){
#
#   echo "declare -A AWS_REGIONS=(" > ${PROJECT_ROOT}/run_script/setup-script/temp_regions.sh
#   aws ec2 describe-regions --query Regions[*].{HP:RegionName} --output text|awk 'NR%2==1 {printf "[\""$0"\"]="} NR%2==1 { print "\""$0"\""}' >> ${PROJECT_ROOT}/run_script/setup-script/temp_regions.sh
#   echo ")" >> ${PROJECT_ROOT}/run_script/setup-script/temp_regions.sh
# #source ${PROJECT_ROOT}/run_script/setup-script/temp_regions.sh
# }

get_aditional_details(){

  TF_VARS_FILES="${TF_INFRA}/${TF_DIRECTORY}/terraform.tfvars"
  [ -f "${TF_VARS_FILES}" ] || touch ${TF_VARS_FILES}
  PRE_REGION="$(grep 'aws_region' ${TF_VARS_FILES}|cut -d '"' -f2)"
  CHECK_ENV_OWNER="$(grep 'Environment Variable' ${TF_VARS_FILES}|rev|cut -d ' ' -f1|rev)"

  if [[ "${TF_VAR_aws_region}" != "${PRE_REGION}" || "${TF_VAR_env_owner}" != "${CHECK_ENV_OWNER}" ]]; then
    #echo > ${TF_VARS_FILES}
    echo "# Environment Variable for ${PROJECT_NAME_CAPS} on ${TF_VAR_env_owner}" > ${TF_VARS_FILES}
  fi

if [[ ! -z ${AWS_DEFAULT_REGION} ]]; then

  subnet_check="$(grep 'subnet_id' ${TF_VARS_FILES}|cut -d '=' -f1)"
  vpc_id_check="$(grep 'vpc_id' ${TF_VARS_FILES}|cut -d '=' -f1)"
  vpc_cidr_check="$(grep 'vpc_cidr' ${TF_VARS_FILES}|cut -d '=' -f1)"
  aws_ami_id_check="$(grep 'aws_ami_id' ${TF_VARS_FILES}|cut -d '=' -f1)"
  aws_region_check="$(grep 'aws_region' ${TF_VARS_FILES}|cut -d '=' -f1)"

  [ ! -z ${aws_ami_id_check} ] || \
    echo "aws_ami_id = \"$(aws ec2 describe-images --filters Name=name,Values=${FIND_AMI} --query 'Images[?Architecture==`x86_64`].[ImageId]' --output text | sort -k2 -r \
    | head -n1)\"" >>  ${TF_VARS_FILES}
  [ ! -z ${subnet_check} ] || echo  "subnet_id = \"$(aws ec2 describe-subnets --query Subnets[0].{ID:SubnetId} --output text)\"" >>  ${TF_VARS_FILES}
  [ ! -z ${vpc_id_check} ] || echo  "vpc_id = \"$(aws ec2 describe-vpcs --query Vpcs[0].{ID:VpcId} --output text)\"" >>  ${TF_VARS_FILES}
  [ ! -z ${vpc_cidr_check} ] || echo  "vpc_cidr = \"$(aws ec2 describe-vpcs --query Vpcs[0].{CIDR:CidrBlock} --output text)\"" >>  ${TF_VARS_FILES}
  [ ! -z ${aws_region_check} ] || echo "aws_region = \"${TF_VAR_aws_region}\"" >>  ${TF_VARS_FILES}
else
  log_error "Please Check AWS REGION in your Env Configs file"
fi

}




fetch_default_aws_details(){
  #TF_VAR_os_name=${1}

  case ${TF_VAR_os_name} in
    ubuntu-18 )
      #FIND_AMI="*hvm-ssd/ubuntu-*-16.04-amd64*"
      FIND_AMI="*ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server*"
      export SSH_USER="ubuntu"
      ;;
    ubuntu-16 )
      #FIND_AMI="*hvm-ssd/ubuntu-*-16.04-amd64*"
      FIND_AMI="*hvm-ssd/ubuntu-xenial-16.04-amd64-server-20180126*"
      export SSH_USER="ubuntu"
      ;;
    ubuntu-14 )
      #FIND_AMI="*hvm-ssd/ubuntu-*-16.04-amd64*"
      FIND_AMI="*ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server*"
      export SSH_USER="ubuntu"
      ;;
    redhat )
      FIND_AMI="RHEL-7.4_HVM-20180103-x86_64-2-Hourly2-GP2"
      ;;
    amazon )
      FIND_AMI=""
      ;;
    windows )
      FIND_AMI=""
      ;;
    * )
      log_error "check variable value : TF_VAR_os_name=${TF_VAR_os_name}"
      exit 1
      ;;
  esac

  if [[ ! -z ${FIND_AMI} ]]; then

    if [[ ${#AWS_REGIONS[@]} -eq 0 ]]; then
      # get_aws_regions
      log_warning "Unable to Load array : AWS_REGIONS"
    else
      CHECK_REGION="${AWS_REGIONS[$AWS_DEFAULT_REGION]}"
      if [[ "${CHECK_REGION}" == "${AWS_DEFAULT_REGION}" ]]; then
        get_aditional_details
      # log_success "Valid Regions"
      else
        log_error "Invalid Region: Please check Region value : ${AWS_DEFAULT_REGION}"
        exit 1
      fi
    fi

  fi
}
