#!/usr/bin/env bash

#if [[ ! -f ${TERRA_BIN_DIR}/terraform ]]; then

download_terraform() {


  case $(getconf LONG_BIT) in
    64)
      ARCH="amd64" ;;
    *)
      ARCH="386" ;;
  esac

  case "$OSTYPE" in
    darwin*)
      OS="darwin" ;;
    linux*)
      OS="linux" ;;
    *)
      OS="Unknown" ;;
  esac

  terraform_version=$(curl -L https://releases.hashicorp.com/terraform/|grep terraform_|grep -v '-'|head -n 1|cut -d"/" -f3)
  download_url="https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_${OS}_${ARCH}.zip"
  terraform_zip="/tmp/terraform.zip"
echo "${download_url}"
#echo "https://releases.hashicorp.com/terraform/0.11.0/terraform_0.11.0_linux_amd64.zip"
  curl "${download_url}" -o "${terraform_zip}"
  unzip "${terraform_zip}" -d "${BIN_DIR}/"
  rm "${terraform_zip}"

  if [[ ! -f "/usr/bin/terraform" ]]; then
    sudo cp -rf ${BIN_DIR}/terraform /usr/bin/
    sudo chmod u+x /usr/bin/terraform
  fi
}


#fi
