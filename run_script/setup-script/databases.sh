#!/usr/bin/env bash

export_terraform_outputs(){
  while IFS=" = " read -r variable_name variable_value
  do
    export ${variable_name}="${variable_value}"
  done < "${LOCAL_ENVIRONMENT_STATE_DIR}/outputs.txt"

}

newpw() {
  local chars=${1}
  < /dev/urandom tr -dc _*A-Za-z0-9 | head -c"${chars}";echo;
}

create_user_and_db(){
  echo "running create_user_and_db"
  export_terraform_outputs
  if [[ ! -z "${rds_db_hostname}" ]]; then
    if [[ "${TF_MODE}" -eq "ansible" && ! -f "${LOCAL_ENVIRONMENT_STATE_DIR}/db_user.sql" ]]; then

      for ((i=0;i<${#RDS_DB_USERS[@]};++i)); do
        RDS_USER="${RDS_DB_USERS[i]}"
        RDS_DB="${RDS_DB_NAMES[i]}"
        RDS_USER_PASS=$(newpw 12)
        echo "DB USER: ${RDS_USER} -- DB USER PASS: ${RDS_USER_PASS} -- DB HOST NAME: ${rds_db_hostname}" >> ${ENV_CONFIG}/db_users.secrets
        cat >> ${LOCAL_ENVIRONMENT_STATE_DIR}/db_user.sql << EOF
      CREATE USER '${RDS_USER}'@'%' IDENTIFIED BY '${RDS_USER_PASS}';
      CREATE DATABASE ${RDS_DB};
      GRANT ALL ON ${RDS_DB}.* TO '${RDS_USER}'@'%';
EOF
      done
      log_success "Creating Database and users with full access on specific database"
      ${MYSQL} -h${rds_db_hostname} -u ${TF_VAR_rds_username} -p${TF_VAR_rds_password} < ${LOCAL_ENVIRONMENT_STATE_DIR}/db_user.sql

     else
       log_alert "db user already Created!! "
    fi
   else
     log_alert "rds_db_hostname variable not found "   
  fi

}
