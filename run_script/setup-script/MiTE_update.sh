#!/usr/bin/env bash

mite_auto_update() {
  log_alert "MiTE updating .."
  if [[ ${AUTO_UPDATE_MiTE} == "true"  ]]; then
    cd ${PROJECT_ROOT}
    echo "This is just for Testing file - MiTE AWS Platform" > ${PROJECT_ROOT}/test.txt
    git stash save -u
    git checkout master
    log_alert "Please enter gitlab login credentials"
    git pull
    git checkout -
    git rebase master
    git stash apply stash@{0} > /dev/null 2>&1
    rm -rf ${PROJECT_ROOT}/test.txt

  else
    echo "Please enable AUTO_UPDATE_MiTE true in run_vars file"
  fi
}


create_mite_backup(){

  rm -rf ${MiTE_BKF}/configs/${PROJECT_NAME}
  rm -rf ${MiTE_BKF}/states/${PROJECT_NAME}

  if [[ ${TF_MODE} != "destroy" ]]; then
    mkdir -p ${MiTE_BKF}/configs/${PROJECT_NAME}/
    mkdir -p ${MiTE_BKF}/states/${PROJECT_NAME}/
    mkdir -p ${MiTE_BKF}/secrets/

    cp -rf ${PROJECT_ROOT}/configs/${PROJECT_NAME}/* ${MiTE_BKF}/configs/${PROJECT_NAME}/
    cp -rf ${PROJECT_ROOT}/states/${PROJECT_NAME}/* ${MiTE_BKF}/states/${PROJECT_NAME}/
    #cp -rf ${PROJECT_ROOT}/run_script/bucket/aws_secret*.sh ${MiTE_BKF}/secrets/${USER}_keys.sh
  fi

  find ${MiTE_BKF} -type d -empty -delete
  cd ${MiTE_BKF}
  git add -A
  git commit -m "Environment ${PROJECT_NAME} has been created Successfully"
  GITX pull
  GITX push -u origin master
}

restore_files(){
  git_repo
    if [[ -d ${MiTE_BKF}/configs/${PROJECT_NAME} && -d ${MiTE_BKF}/states/${PROJECT_NAME} ]]; then
      log_alert " Restoring Backup ..."
      cp -rn ${MiTE_BKF}/configs/${PROJECT_NAME} ${PROJECT_ROOT}/configs/
      cp -rn ${MiTE_BKF}/states/${PROJECT_NAME} ${PROJECT_ROOT}/states/
    # else
    #   log_alert " No Backup Available !!!"
    fi
}
