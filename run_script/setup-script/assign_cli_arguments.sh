#!/usr/bin/env bash

assign_cli_arguments() {

  while [[ $# -gt 0 ]]; do
    case "${1}" in
      -h|--help)
        usage ;;
      -u|--update)
        mite_auto_update
        exit 1 ;;
      -e|--environment)
        PROJECT_NAME=$(echo ${2} | tr '[:upper:]' '[:lower:]')
        export PROJECT_NAME
        shift 2 ;;
      -y|--yes)
        TF_RUN_YES="yes"
        shift ;;
      --plan|--apply|--destroy|--deploy|--ansible|--config|--gen|--manage|--test)
        TF_MODE="${1//--/}"
        shift ;;
      --playbook)
        ANSIBLE_PLAYBOOK=${2}
        shift 2 ;;
      -s|--ssh)
        SSH_HOST_IP=${2}
        shift 2 ;;
      -t|--tags)
        ANSIBLE_TAGS=${2}
        shift 2 ;;
      -c|--check)
        ANSIBLE_OPTIONS+=" --check"
        shift ;;
      -d|--diff)
        ANSIBLE_OPTIONS+=" --diff"
        shift ;;
      -v|--verbose)
        ANSIBLE_OPTIONS+=" --verbose"
        shift ;;
      # --ssl-gen)
      #   CONFIG_NAME=${2}
      #   shift 2 ;;
      --env-gen|--env|--ssl-gen|--ssl)
        CONFIG_MODE="${1//--/}"
        CONFIG_NAME=${2}
        shift 2 ;;
      *)
        echo -e "${RED}FATAL: Unknown command-line argument or environment: ${1}${NC}"
        exit 1
    esac
  done
}
