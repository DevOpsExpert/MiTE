#!/usr/bin/env bash

configure_instance (){
#  log_info "running ${FUNCNAME[*]}"
    HOSTS_IP=()
    #output_ips=( PUBLIC_IPS PUBLIC_IPS PUBLIC_IPS )

    ENV_CHECK=$(${JQ} .modules[0].outputs.${output_ips[0]}.value ${STATE_FILE})
    #echo ENV_CHECK: ${ENV_CHECK}
    # Uncomment if you want to reset host file.
    # cat ${ANSIBLE_HOSTS}/hosts.example > ${ANSIBLE_HOSTS_FILE}

    if [[ "${ENV_CHECK}" != "null" ]]; then

    for ip in "${output_ips[@]}"
    do
      HOSTS_IP[${#HOSTS_IP[*]}]=$($TERRAFORM output -state="${STATE_FILE}" $ip)
    done

    if [[  -z ${HOSTS_IP} ]]; then
      echo ; log_error "Please Create Invironment first."
       exit 0
    fi

    if [[ ! -f ${ANSIBLE_HOSTS_FILE}  ]]; then
      cp ${ANSIBLE_HOSTS_TMPL} ${ANSIBLE_HOSTS_FILE}
    fi

     add_ansible_hosts(){
       STR_CHECK=${1%?}
       AP="$2"
     local ip_check=$(grep -a5 ${STR_CHECK} ${ANSIBLE_HOSTS_FILE} | grep "${HOSTS_IP[${AP}]}")
      [ ! -z "${ip_check}" ] || sed -i '/'${STR_CHECK}'/a '${HOSTS_IP[AP]} ${ANSIBLE_HOSTS_FILE}
    }

    # Add More IP That You want to add in ansible hosts file.
    for KEY in "${!ANSIBLE_HOST_ADD[@]}"; do
    # add_ansible_hosts "web" 0
    # Available Host Groups: web db others
    add_ansible_hosts $KEY ${ANSIBLE_HOST_ADD[$KEY]}
    done

    for ((i=0; i<${#HOSTS_IP[*]}; i++));
    do
      #  echo "HOSTS_IP[$i] = ${HOSTS_IP[$i]}";
      echo "$i : ${HOSTS_IP[$i]}";

      # This Will Add all Outputs in [all] Groups in ansible hosts
      local ip_check=$(grep -a5 all ${ANSIBLE_HOSTS_FILE} | grep "${HOSTS_IP[$i]}")
      if [[ -z ${ip_check} ]]; then
        sed -i '/all/a '${HOSTS_IP[$i]} ${ANSIBLE_HOSTS_FILE}
      fi
    done




 ssh_connection(){
    if [[ ${SSH_HOST} = *[0-9]* && ! -z ${SSH_HOST} && ${SSH_HOST} -lt ${#HOSTS_IP[*]}  ]]; then
        #  echo "ssh -F ${ANSIBLE_SSH_CONFIG_FILE} ${SSH_USER}@${HOSTS_IP[$SSH_HOST]} < ${PROJECT_ROOT}/run_script/bucket/install_on_remote.sh"
          ssh -T -F ${ANSIBLE_SSH_CONFIG_FILE} ${HOSTS_IP[$SSH_HOST]} < ${PROJECT_ROOT}/run_script/bucket/install_on_remote.sh
        else
          log_alert "Skipping SSH Connections"
    fi
  }

  if [[ -z ${ANSIBLE_PLAYBOOK} ]]; then
    echo;
    echo "Continue without SSH : Press ENTER"
    read -e -p "Please Number That you want to connect SSH [0-$(expr ${#HOSTS_IP[*]} - 1)] : " SSH_HOST
    ssh_connection
  else
    for ((i=0; i<${#HOSTS_IP[*]}; i++));
    do
    local SSH_HOST="${i}"
    ssh_connection
  done
  fi

  else
    log_warning "Environment Not Created !!"
  fi
}

hosts_variables(){
  #log_alert "updating extra variables in hosts file "
    for ENV_NAME in ${INIT_ENVIRONMENT[@]}; do
      UP_ENV=$(echo ${ENV_NAME}|tr '[:lower:]' '[:upper:]')
      LW_ENV=$(echo ${ENV_NAME}|tr '[:upper:]' '[:lower:]')
      ENV_PUB_IP=$(${JQ} .modules[0].outputs.${UP_ENV}_PUB_IP.value ${STATE_FILE}|cut -d '"' -f2)
      ANSIBLE_ENV_VARS=()
      ANSIBLE_ENV_VARS+=("env_name=${LW_ENV}")
      get_value="${LW_ENV}_"

      ENV_VARIABLES=$(for i in ${!ANSIBLE_ENV_DEPENDANT_VARS[@]}; do  echo $i; done |grep -i "${get_value}")
      for KEY in ${ENV_VARIABLES}; do ANSIBLE_ENV_VARS+=("${KEY//${get_value}}=${ANSIBLE_ENV_DEPENDANT_VARS[$KEY]}") ;done

      VARIABLE_DATA="${ENV_PUB_IP} ${ANSIBLE_ENV_VARS[@]}"
      sed -i -e 's|'".*${ENV_PUB_IP}.*"'|'"${VARIABLE_DATA}"'|g' "${ANSIBLE_HOSTS_FILE}"
      ret_value_check=$?
      if [[ ${ret_value_check} = "0" ]]; then
        log_success "ansible hosts updated Successfully"
      else
        log_error "something went wrong"
        exit 1
      fi

    done

}


config_and_run_ansible(){
  if [[ ! -f ${ANSIBLE_HOSTS}/${PROJECT_NAME}_hosts ]]; then
    configure_instance
    hosts_variables
  else
    hosts_variables
  fi
}
