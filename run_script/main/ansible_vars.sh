#!/usr/bin/env bash

add_vars_in_ansible(){
  # Any time we can call this FUNCNAME to inject vars in ansible
  # add_vars_in_ansible "AGE" "27"

  KEY="$1"
  VALUE="$2"
  log_alert "Importing Variables = ${KEY}: ****${VALUE: -4}"
  ANSI_VARS[${KEY}]="$VALUE"
}


static_ansible_variables(){
add_vars_in_ansible "OFFICIAL_EMAIL" "${TF_VAR_email}"
add_vars_in_ansible "WEB_ENVRIONMENT" "${WEB_ENVRIONMENT}"
add_vars_in_ansible "php_version" "${PHP_VERSION}"
add_vars_in_ansible "project_root" "${PROJECT_ROOT}"
add_vars_in_ansible "project_environment" "${PROJECT_NAME}"

}



import_shell_vars_in_ansible(){
echo "Importing Variables in ansible"
static_ansible_variables

for K in "${!ANSI_VARS[@]}";
do
   VARS_CHECK=$(cut -d"=" -f1 ${LOCAL_ENVIRONMENT_STATE_DIR}/outputs.txt|grep "$K")
  if [[ -z ${VARS_CHECK} ]]; then
    #echo "Added Vars $K = ${ANSI_VARS[$K]}"
    echo "$K = ${ANSI_VARS[$K]}" >> "${LOCAL_ENVIRONMENT_STATE_DIR}/outputs.txt"
  fi

done

}

timediff() {

  #datediff "h|m|d" "date or time in second"
  #START=$(date -d "${2}" '+%s')
  START=${2}
  END=$(date +%s)
  #END=START=$(date -d "${2}" '+%s')

case ${1} in
  M|m )
    #Minutes
    DIFF=$(( (END - START) / 60 ))
    ;;
  H|h )
    # Hour
    DIFF=$(( (END - START) / 3600 ))
    ;;
  D|d )
    # Day
    DIFF=$(( (END - START) / 86400 ))
    ;;
esac
echo "${DIFF}"
}
