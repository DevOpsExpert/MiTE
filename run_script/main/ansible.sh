#!/usr/bin/env bash
ansible_execution(){
  echo "Executing ${ANSIBLE_PLAYBOOK}"

  # ansible -i ${ANSIBLE_HOSTS}/hosts -m ping -all
  #  ansible all -m ping -i ${ANSIBLE_HOSTS}/hosts --private-key=${ANSIBLE_PRIVATE_KEY} -u ec2-user
  #  ansible-playbook ansible/playbook/test.yml -i ansible/hosts/hosts --private-key=configs/test/test -u ubuntu
  #echo ${ANSIBLE_PLAYBOOK_BIN} ${PLAYBOOK_DIR}/${ANSIBLE_PLAYBOOK} -i ${ANSIBLE_HOSTS}/hosts --private-key=${ANSIBLE_PRIVATE_KEY}

  if [[ ! -z ${ANSIBLE_PLAYBOOK} ]]; then
    ${ANSIBLE_PLAYBOOK_BIN} ${PLAYBOOK_DIR}/${ANSIBLE_PLAYBOOK} -i ${ANSIBLE_HOSTS_FILE} --private-key=${ANSIBLE_PRIVATE_KEY}
  fi
}


write_ssh_config() {
  #log_info "Running: write_ssh_config"

  if [[ "${USER}" == "jenkins2" ]]; then
    echo "Please config ssh configs file.- This will automate soon.- Harry"
    #  cp "${ANSIBLE_SRC_JENKINS_SSH_CONFIG_FILE}" "${ANSIBLE_SSH_CONFIG_FILE}"
  else
    cp "${ANSIBLE_SRC_SSH_CONFIG_FILE}" "${ANSIBLE_SSH_CONFIG_FILE}"
  fi

  sed -e 's|{TF_VAR_deploy_private_key}|'"${TF_VAR_private_key}"'|g'     \
    -e 's|{ANSIBLE_SSH_CONFIG_FILE}|'"${ANSIBLE_SSH_CONFIG_FILE}"'|g'    \
    -e 's|{SSH_USER}|'"${SSH_USER}"'|g'    \
    --in-place=.bak "${ANSIBLE_SSH_CONFIG_FILE}"

  rm "${ANSIBLE_SSH_CONFIG_FILE}.bak"
}



create_ansible_cfg() {
  #log_info "Running: create_ansible_cfg"

  cp "${SRC_ANSIBLE_CFG}" "${ANSIBLE_CFG_FILE}"

  sed -e 's|{PROJECT_NAME}|'"${PROJECT_NAME}"'|g'     \
    -e 's|{ANSIBLE_SSH_CONFIG_FILE}|'"${ANSIBLE_SSH_CONFIG_FILE}"'|g'    \
    -e 's|{SSH_USER}|'"${SSH_USER}"'|g'    \
    -e 's|{ANSIBLE_HOSTS_FILE}|'"${ANSIBLE_HOSTS_FILE}"'|g'    \
    --in-place=.bak "${ANSIBLE_CFG_FILE}"

  rm "${ANSIBLE_CFG_FILE}.bak"
}


ansible_set_extravars() {
  log_info "running ${FUNCNAME[*]}"
#  echo "print ${output_ips[@]}"
if [[ -f ${STATE_FILE} ]]; then
  ENV_CHECK=$(${JQ} .modules[0].outputs.${output_ips[0]}.value ${STATE_FILE})
else
  log_alert "MiTE environment does't exists."
fi

  #echo ENV_CHECK: ${ENV_CHECK}
  if [[ "${ENV_CHECK}" != "null" ]]; then
    # pushd ${TF_INFRA}/${TF_DIRECTORY}
    cd ${TF_INFRA}/${TF_DIRECTORY}
    OUTPUT_FILE="${LOCAL_ENVIRONMENT_STATE_DIR}/outputs.txt"
    # echo "RUN: Processing Ansible variables from Terraform..."


     if [[ -f ${OUTPUT_FILE} ]]; then
       LAST_MODIFIED=$(stat --format=%Y ${OUTPUT_FILE})
       HOUR_DIFF=$(timediff "h" "${LAST_MODIFIED}")
       if [[ "${HOUR_DIFF}" -gt "1" ]]; then
         echo; log_alert "Fetching Latest infra Info and variables"
         rm "${OUTPUT_FILE}"  > /dev/null 2>&1
         $TERRAFORM refresh -state="${STATE_FILE}"
         $TERRAFORM output -state="${STATE_FILE}" > "${OUTPUT_FILE}"
       fi
     fi

    import_shell_vars_in_ansible
    echo
    # echo
    # echo "Terraform outputs:"
    # cat "${LOCAL_ENVIRONMENT_STATE_DIR}/outputs.txt"
    # echo "RUN: Processing Ansible variables from Terraform..."

    rm "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json" > /dev/null 2>&1
    echo "{" >> "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json"

    while IFS=" = " read -r output_name output_value
    do
    #  echo "OUTPUT_NAME: OUTPUT_VALUE"
    #  echo "${output_name}: ${output_value}"
      echo "\"${output_name}\": \"${output_value}\"," >> "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json"
    done < "${LOCAL_ENVIRONMENT_STATE_DIR}/outputs.txt"

    echo "}" >> "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json"
    # echo
    # echo
    #cat "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json"

    # popd
  else
    log_warning "Environment Does Not Exists !!"
    export ANSIBLE_HOSTS_FILE="${ANSIBLE_HOSTS}/hosts"
  fi
  #import_shell_vars_in_ansible
}

# load_env_vars_in_ansible(){
#
#   sed -i -e "/}/d" "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json"
#   sed -i "s/\"//g" "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_env_vars"
#
#   while IFS="=" read -r variable_name variable_value
#   do
#       echo "variable_name: ${variable_name}"
#       echo "variable_value: ${variable_value}"
#       echo "\"${variable_name}\": \"${variable_value}\"," >> "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json"
#   done < "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_env_vars"
#
#   echo "}" >> "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json"
# }


ansible_run_playbooks() {
  #echo "RUN: Running Ansible playbooks:"
  checking_ansible

  local ansible_playbook
  export ANSIBLE_RETCODE

  ansible_playbook="${1}"

  if [[ -z "${ansible_playbook}" ]]; then
    log_error "No playbook option specified."
    ANSIBLE_RETCODE=1

  else

    ansible_playbook="${PLAYBOOK_DIR}/${ansible_playbook}"

    # echo "RUN:   ${ansible_playbook}"
    # echo "AWS_PROFILE: ${AWS_PROFILE}"
    # echo "AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}"
    # echo "AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY}"
    # echo "AWS_SECURITY_TOKEN: ${AWS_SECURITY_TOKEN}"
    # echo "AWS_SESSION_TOKEN: ${AWS_SESSION_TOKEN}"

    # log_info "ANSIBLE_CONFIG_FILE: ${ANSIBLE_CFG_FILE}"
    # log_info "ANSIBLE_SSH_CONFIG_FILE: ${ANSIBLE_SSH_CONFIG_FILE}"
    # log_info "ANSIBLE_HOSTS_FILE: ${ANSIBLE_HOSTS_FILE}"
    # log_info "VPC_BASTION_IP: ${VPC_BASTION_IP}"


    cp "${LOCAL_ENVIRONMENT_STATE_DIR}/ansible_extra_vars.json" "${ANSIBLE_DIR}/ansible_extra_vars.json"

    # gitcrypt_unlock
    # set_ssh_key_permissions
    cd ${ANSIBLE_DIR}

    ANSIBLE_CONFIG="${ANSIBLE_CFG_FILE}" ${ANSIBLE_PLAYBOOK_BIN} --inventory-file=${ANSIBLE_HOSTS_FILE} \
      --ssh-common-args="-F ${ANSIBLE_SSH_CONFIG_FILE}"  \
      "${ansible_playbook}" \
      --extra-vars "@ansible_extra_vars.json" \
      ${ANSIBLE_TAGS:+--tags ${ANSIBLE_TAGS}} \
      ${ANSIBLE_OPTIONS}


    ANSIBLE_RETCODE=${?}
    #gitcrypt_lock

  fi

  log_info "ANSIBLE_RETCODE: ${ANSIBLE_RETCODE}"
}
