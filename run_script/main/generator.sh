# ./main.sh -e {environment_name} --create --env-config {config_name}

resource_creator(){
  log_info "running ${FUNCNAME[*]}"
  case ${CONFIG_MODE} in
    env-config|env )
      log_alert "Creating New Environment config Files for :${CONFIG_NAME}"
      bash ${PROJECT_ROOT}/utils/create_environment.sh "${CONFIG_NAME}"
      ;;
    ssl-gen|ssl )
      log_alert "Generating SSL Certificate for :${CONFIG_NAME}"
      create_ssl_cert "${CONFIG_NAME}"
      ;;
  esac

}


create_ignored_directory(){
mkdir -p ${PROJECT_ROOT}/{tmp,ssl_cert,bin}

}
create_ignored_directory
