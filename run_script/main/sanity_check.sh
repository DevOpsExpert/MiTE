#!/usr/bin/env bash

sanity_check(){
  log_info "running ${FUNCNAME[*]}"
  echo "TERRAFORM Verion: $(${TERRAFORM} -v)"
}

ssh_key_check(){
  if [[ ! -f "${HOME}/.ssh/ssh_config" ]]; then
  cat >${HOME}/.ssh/ssh_config <<EOF
    Host *
      ForwardAgent yes
      ForwardX11 no
      ConnectionAttempts 3
      ConnectTimeout 60
      TCPKeepAlive yes
      ServerAliveCountMax 6
      ServerAliveInterval 30
      ControlMaster auto
      ControlPath /tmp/.sshC.%u.%h.%p.%r
      ControlPersist 1h
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
EOF

  fi
}
