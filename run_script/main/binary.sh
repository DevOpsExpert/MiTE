#!/usr/bin/env bash

export ANSIBLE="$(which ansible)"
export ANSIBLE_PLAYBOOK_BIN="$(which ansible-playbook)"
export AWS_CLI="$(which aws)"
export MYSQL="$(which mysql)"


if [[ -z ${AWS_CLI} ]]; then
  log_alert "Installing AWSCLI..."
  sudo apt install awscli -y
export  AWS_CLI="$(which aws)"
fi

export TERRAFORM="$(which terraform)"
if [[ -z ${TERRAFORM} ]]; then
  export TERRAFORM="${BIN_DIR}/terraform"
fi

export JQ="$(command -v jq)"
if [[ -z ${JQ} ]]; then
  sudo apt-get install jq -y
  export JQ="$(command -v jq)"
fi
UNZIP="$(command -v unzip)"
if [[ -z ${UNZIP} ]]; then
 sudo apt-get install unzip -y
fi

checking_ansible(){
  if [[ -z ${ANSIBLE} ]]; then
    log_alert "Please wait.. Installing Ansible"
    sudo apt-get update -y
    sudo apt-get install software-properties-common -y
    sudo apt-add-repository ppa:ansible/ansible -y
    sudo apt-get update -y
    sudo apt-get install ansible -y
    export ANSIBLE="$(which ansible)"
    export ANSIBLE_PLAYBOOK_BIN="$(which ansible-playbook)"
  fi

}
