export_local_vars(){
export import_local_variables=$(sed '/./!d' ${LOCAL_ENV_CONFIG_FILE}|sed '/^\s*#/d')
sed '/./!d' ${LOCAL_ENV_CONFIG_FILE}|sed '/^\s*#/d'|sed -e 's/^/export /' > ${TMP_DIR}/${PROJECT_NAME}_initial_modified_configs.sh
source "${TMP_DIR}/${PROJECT_NAME}_initial_modified_configs.sh"
#echo "project name export_local_vars : ${NEW_PROJECT_NAME}"

}

check_rds_name(){
for (( j=0; $j < ${#INIT_ENVIRONMENT[@]}; j+=1 ));
do
  KEY=${INIT_ENVIRONMENT[$j]};
  echo $KEY
done | grep -i "${1}"
}

get_rds_name(){
rds_name=$(check_rds_name "prod" )

if [[ ! -z ${rds_name} ]]; then

  case ${rds_name} in
    prod|production )
    export TF_VAR_rds_env_name="production"
      ;;
    dev|qa|stage )
      export TF_VAR_rds_env_name="dqs"
      ;;
    * )
      export TF_VAR_rds_env_name="dqs"
      ;;
  esac

else
  export TF_VAR_rds_env_name="dqs"
fi


}

load_extra_variables(){
  # Here you can add extra variable it will execute in last.
  export TF_VAR_project_key=$(echo ${TF_VAR_project_name}|sed  's/ /-/g')
  get_rds_name
}
