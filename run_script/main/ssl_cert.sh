#!/usr/bin/env bash

generate_self_signed_ssl(){
    local ssl_type=$1
    local csr=$2
    local cert=$3
    local private_key="${SSL_KEY}"
    local exit_val


    if [[ ! -f "${SSL_KEY}" || ! -f "${SSL_CSR}" || ! -f "${SSL_CRT}" ]] ; then

        local cert_exit_val
        # certificate details
        local country="IN"                         # 2 letter country-code
        local state="Delhi"                        # state or province name
        local locality="Delhi"                     # Locality Name (e.g. city)
        local orgname="hari-shankar.com"           # Organization Name (eg, company)
        local orgunit="HS-Group"                   # Organizational Unit Name (eg. section)
        local cname="server1.deo.com"

        log_info "Generating Certificate Signing Request for ${FILE_NAME}.csr"
        openssl req -new -sha256 -key "${SSL_KEY}" -out "${SSL_CSR}" -subj "/C=${country}/ST=${state}/L=${locality}/O=${orgname}/OU=${orgunit}/CN=${cname}"

        #Automate generation of self signed SSL certificate for freeipa
        log_info "Generating Self signed certificate for ${FILE_NAME}.crt"

        openssl req -x509 -sha256 -days 365 -key "${SSL_KEY}" -in "${SSL_CSR}" -out "${SSL_CRT}"
        cert_exit_val=${?}

        if [[ -f "${SSL_CRT}" ]]; then
            log_success "CSR has been created - ${FILE_NAME}.csr "
            log_success "Certificate has been created - ${FILE_NAME}.crt "

        fi

    else

        log_info "Using existing Self signed certificate for ${ssl_type}"

    fi
}


create_ssl_cert () {
    CONFIG_NAME=$1
    if [[ -z ${CONFIG_NAME} ]]; then
        read -e -p "Please Enter Cert Name: " CONFIG_NAME
    fi

    FILE_NAME=${PROJECT_NAME}_${CONFIG_NAME}
    SSL_KEY="${SSL_DIR}/${FILE_NAME}.key"
    SSL_CSR="${SSL_DIR}/${FILE_NAME}.csr"
    SSL_CRT="${SSL_DIR}/${FILE_NAME}.crt"

    if [[ ! -f "${SSL_KEY}" || ! -f "${SSL_CSR}" || ! -f "${SSL_CRT}" ]]; then
        echo "Missing private key or certificate"
        echo "Hance creating ssl key,csr & crt"
        openssl genrsa -out "${SSL_KEY}" 2048
        generate_self_signed_ssl

    else
        log_alert  "All certs available"
    fi
}
