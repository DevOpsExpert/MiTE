#!/usr/bin/env bash
export STATE_FILE=${STATE_DIR}/${PROJECT_NAME}/terraform.tfstate
export STATE_BKF_FILE=${STATE_FILE}.backup

terraform_run() {
  [[ "${TF_MODE}" == "plan" ]] && exit 0

  if [[ "${TF_RUN_YES}" != "yes" ]]; then
    echo; read -e -p "Are you happy with this plan (yes/no)? " TF_RUN_YES
    [[ "${TF_RUN_YES}" != "yes" ]] && exit 0
  fi

  echo
  echo "RUN: Applying Terraform with state file ${STATE_FILE}"

  if [[ "${TF_MODE}" == "destroy" ]]; then
    cleanup_environment_hosts
    $TERRAFORM destroy -force -state="${STATE_FILE}"
    return_val=$?
  #  echo "return_val: ${return_val}"
    [[ ${return_val} -ne 0 ]] && \
      echo -e "${RED}WARNING: Terraform plan not applied successfully.${NC}" >&2 && \
      exit 1
    save_tfstates

  else
    $TERRAFORM apply -state="${STATE_FILE}" -input=false -auto-approve
    ret_val=$?
    echo "ret_val: ${ret_val}"
    [[ ${ret_val} -ne  0 ]] && \
      echo -e "${RED}WARNING: Terraform plan not applied successfully.${NC}" >&2 && \
      exit 1
  save_tfstates
  fi
}


terraform_execute(){
  cd ${TF_INFRA}/${TF_DIRECTORY}
  # echo "fetching modules"
  ${TERRAFORM} get

  if [[ ! -d ${TF_INFRA}/${TF_DIRECTORY}/.terraform/plugins  ]]; then
    ${TERRAFORM} init
  fi

  if [[ ${TF_MODE} == destroy ]]; then
    echo "Created Infra state Backup"
    cp ${STATE_FILE} ${STATE_BKF_FILE}

    echo "Destroying terraform code : ${PROJECT_NAME}"
    ${TERRAFORM} plan -destroy -state="${STATE_FILE}" -detailed-exitcode
    TF_RETVAL=${?}

  else
    echo "Executing Plan ${PROJECT_NAME}"
    ${TERRAFORM} plan -state=${STATE_FILE} -detailed-exitcode
    TF_RETVAL=${?}
  fi


  case ${TF_RETVAL} in
    0)
      msg="SUCCESSFUL: No updates required by Terraform."
      echo; echo "${msg}"
      ;;
    1)
      msg="FATAL: Error in the Terraform plan. Cannot continue."
      echo; echo -e "${RED}${msg}${NC}"
      exit 3
      ;;
    2)
      terraform_run
      ;;
    *)
      echo " invalid return code."
      ;;
  esac
}
