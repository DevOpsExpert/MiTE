#!/usr/bin/env bash

cleanup_environment_hosts(){
  #log_info "running : ${FUNCNAME[*]}"
  log_alert "Removing unused files"
  rm -rf ${ANSIBLE_HOSTS_FILE} ${PLAYBOOK_DIR}/*.retry ${ANSIBLE_DIR}/ansible_extra_vars.json
  rm -rf "${LOCAL_ENVIRONMENT_STATE_DIR}/{db_user.sql,ansible_extra_vars.json,outputs.txt}"
}
