install_python(){
#sudo apt-get update -y
sudo apt-get install python-minimal -y
}

declare -A SSH_KEYS=(
["Jenkins"]="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3d/vSaRTYBPbhaEH56eSWge9PTxBUM+mXN1Ajc9aZgu4tA6gsXq5Hk3iJF2Nf7DcbeiYjWjchiK4O54cUbgLpiKm7liRdM9lYsj6wfK8FszIc4KehxCE9qmr/cgZ81B7wxrNM7qU9CXpffokvMS2l+5QCl9P3IJWZgH3AOZyvVWy/XhhcER99XTC1DGnYu/WNyIdfOv+oq5EKnsaAytfcUOspWdFAH5Fpvv96Z77xMAh8Rj26PVAIW7x3bzomZ+27+0uJQg0UdZWUZ1odTt/kLYCcIu8BqHleQrrzN+HrWI2RN4nwBAx49n2QH7xOYM2tDsPveEnWDKEtEpvzCLuD ubuntu@kiwidevops-ci"
["JumHost"]="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQChPl00nWhP0YV7OmZW66bpnN0/lzK9QDephrBxnHR1fd4FZSogFjHKbjb0/XA8/D9l2p0u7E8jcKNa9Wgwn+cYZQNgA8aEQG8VP7uXyJzjamKwo4r3IYDbtrhaxGDOPkrBKYrHN4sOV3AxOA1j9m6spjOWUg8Ok3H0Thy+r+HCBwKCODAwEViWuB6ZUcHWn0PEoC3kK0mR8B4wdRA5cZENVR0mHsShrfsxqiRDqLaRi+BUeobQe21WSRM15eZIv0OvD5j19jGUc22s2hnB3v8u1RPB2DSJpLc/ZateYWhjz4lZe6/hnZu4r8wpyuXQAb/fGAib5FNvct6v3jYyxE4t ubuntu@ip-172-30-1-135"
);

add_ssh_key(){
  for i in ${!SSH_KEYS[@]};
  do
    KEY="$(echo ${SSH_KEYS[$i]}|cut -d' ' -f2)"
    KEY_CHECK="$(grep "${KEY}" ~/.ssh/authorized_keys)"
    if [[ -z ${KEY_CHECK} ]]; then
      echo "Adding ${i} SSH Public key"
      echo "${SSH_KEYS[$i]}" >> ~/.ssh/authorized_keys
    else
      echo "${i} : SSH Key already added"
    fi
  done
}





## Add Functions Above this line
#echo "Please Wait ... Installing Python"
install_python 2>/dev/null
## Adding ssh key
add_ssh_key

#cat ~/.ssh/authorized_keys
