#!/usr/bin/env bash

# This is Static file Environment variable will not work on remote machine.
# This file will transfer on Remote Machine and it will execute on the remote machine at the time of ec2 instance creatation.
# This will Execute only one time.  So you can define one time task in this script.
# This is Standalone static script will execute on Remote machine.
# No Custome variable will work here.
###################################################################===By Harry - The DevOps Guy===##############################
TF_VAR_project=$1
ENVIRONMENT=$2

#sudo apt-get -y update
# sudo apt-get -y install python-minimal
echo "Hello World - ${TF_VAR_project} ENVIRONMENT-${ENVIRONMENT}"
echo "Hello World - ${TF_VAR_project} ENVIRONMENT-${ENVIRONMENT}" > /tmp/harry.txt


# cat > ~/run.sh << EOF
# sudo hostnamectl set-hostname ${TF_VAR_project}-${ENVIRONMENT}
# sudo -- bash -c "echo ${TF_VAR_project}-${ENVIRONMENT} > /etc/hostname"
# EOF
# sudo chmod u+x ~/run.sh
