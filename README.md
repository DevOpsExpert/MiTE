
# Welcome To M!TE AWS Platform (MAP)

This is My one of the best software that is very useful for all devops engineers as well as it is very useful of any !T Company who manging AWS Infra. We can Mange AWS Cloud very easy way with the help of this Tool.

### What is this repository for? ###

* Create AWS Infrastructure and code deployment.
* Version 0.1.9
* [Learn More About This Project ](https://Harry-TheDevopsGuy.blogspot.com)

### Repository URL and Dependencies ?
|  Name| Details |
| ------ | ------ |
|**Project Name**   | M!TE AWS PLATFORM  |
|**Git URL**   | https://gitlab.com/DevOpsExpert/MiTE.git  |
|**Release**   | 0.1.9  |


#### Package Dependencies
| Package | Verison | Remarks|
| --------| --------| ------ |
|Git   | 2.7.4   | or any latest version|
|Git-Crypt   | 0.6.0  | [latest version](https://github.com/AGWA/git-crypt)|
| GPG  | 1.4.20  | Or any latest version  |
|Python 2.7   | latest version  | any latest version  |
|pip   | latest version  |   |
|virtual env   | latest version  |   |
|sed   |  latest version |   |
|openssl   | latest version  |   |
|bash  | 4  |   |
|awscli | latest version  |   |
|tar | latest version  |   |

### How To Run and Create Infrasture on AWS Cloud?

* Login Your AWS A/c and Generate **Aaccess key** and **Secrete key**
* Generate your GPG key upload your key on [**MIT PGP PUBLIC KEY SERVER**](https://pgp.mit.edu/). This will required to Unlock This Repository.
* Send Email at **HarryTheDevopsGuy@gmail.com** to Allow you to Unlock This repo with your private GPG key.
* once your public gpg key will add into this repository you will able to unlock this repository.

### How To Install MiTE ?

```bash
$ curl -L 'https://elbo.in/MiTE'|bash - && source ~/.bashrc
```

### Getting Start ?
once Mite has been installed successfully. then we can check by executing below commands.

```bash
MiTE -h
```


### How To Configure Server?
After completing above step successfully now we can configure web server or install any package on remote machine. using **ansible playbook**.
```sh
$ MiTE -e {your_env_name} --ansible --playbook {your_playbook_name.yml}
```
> before executing this command we must have to add ip in ansible host group.
> You must export ip from terraform `outputs.tf`  and pass this output name in your environmet `.config` file.

```bash
$ cat outputs.tf

output "PUBLIC_IPS" {
  value = "${aws_instance.miniweb.public_ip}"
}
```
Copy output name **PUBLIC_IPS**  and paste into your environment configs as shown below.

```bash
$ cat configs/web/web.config

# This is Example output that we want to add ansbile hosts file in web db etc group.
output_ips=( PUBLIC_IPS PUBLIC_IPS ) # output position 0 1
declare -A ANSIBLE_HOST_ADD=(
["web"]="0"   # This means PUBLIC_IPS (0) public ip will add under web group in ansible host file.
["web"]="1"
["db"]="1"
["others"]="1"
);
```
> web, db, others, all these are group in your Ansible hosts file. this host file will be separate for all environment. host file name format will be {your_env_name}_hosts. this file will create automatically. No need to modify manual.
> You just need to pass outputs.tf name in environment configs. as shown above.

There are Many more variable in environment `.config` file you just need to understand one by one. or i will explain all in future.


---

---

#### How to create **self signed ssl** Certificates?
Sometime we need to need to configure on any site so here command that will create **ssl Certificates** for you automatically.
```bash
$ MiTE -e {your_env_name} --create --ssl-gen {your_ssl_name}
```
```bash
$ MiTE -e web --create --ssl-gen testssl

Creating key/ssl certs
Missing private key or certificate
Hence creating ssl key,csr & crt
Generating RSA private key, 2048 bit long modulus
....................+++
......................................................................+++
e is 65537 (0x10001)
Generating Certificate Signing Request for web_testssl.csr
Generating Self signed certificate for web_testssl.crt
 CSR has been created - web_testssl.csr  
 Certificate has been created - web_testssl.crt
```
>You will get output just like above. it means you have create ssl Certificates in your **ssl_cert** directory.

#### How to Create SSL Certificate? ###
```bash
$ MiTE -e {existing_env_name} --gen|--config --ssl|--ssl-gen <SSL_NAME>
```

SSL certificate is creating at the time of environment creation. but here is another
option to create ssl only. if we want to create ssl without creating environment.

#### How  To Create New Environment Config files?
```bash
$ MiTE -e {existing_env_name} --gen --env {config_name}
$ MiTE -e {existing_env_name} --config --env-gen {config_name}
```
---


#### Example To Create Environment on AWS and configure web server with ssl :
Here we will launce 1 ec2 instance on aws cloud and will install and configure nginx web server and we will point out this web server with some domin. we will also configure letsEncrypt SSL on domain name.

##### Pre Requisite :
    1. Environment requirement file.
    2. Must Have "MiTE AWS Platform" Access.
    3. Must have Basic Knowledge about MiTE AWS Platform Tools
    4. One Domain Name eg. www.HarryTheDevOpsGuy.ml


#### How we can Create Instance and configure nginx ?:
  - To Create configration file with Your Environment Requirement file.
  - Environment config file Name must be like "harry_initial_configs.sh" and it must be save under "~/AWS_ENVIRONMENT_CONFIGS/harry_initial_configs.sh"
  - then Run Below command it will create config files and it will prepare Environment for you.
  - `./MiTE -e harry --plan -y` it used to Create configs file if not created already.
  - `./MiTE -e harry --apply -y`it used to Create resource on your aws cloud.
  - `./MiTE -e harry --ansible --playbook lemp.yml ` It used to update,upgrade system and install nginx, and deploy application you.
  - `./MiTE -e harry --ansible --playbook ssl.yml` ssl.yml is playbook to install & configure ssl.

#### So Guys We need to execute these commands one by one
``` bash
./main -e harry --plan -y
./main -e harry --apply -y
./main -e harry --ansible --playbook lemp.yml
./main -e harry --ansible --playbook ssl.yml
```


[![This MiTE AWS Platform](https://2.bp.blogspot.com/-iFbVjR_VfRE/Wr8xRG8q8-I/AAAAAAAAANo/r4X146GMlDoNfJ2YFVJVMXIXb5zWX4xsgCLcBGAs/s640/harryTheDevopsGuy.png)](https://www.youtube.com/watch?v=Z9g5uwctJBQ "Click Here To Play Video")


---
#### Credits and Terms of Use MiTE AWS Platform
  * you can use **MiTE AWS PLATFORM** free of cost in commercial or non commercial project on your own risk.
  * We have tested on These code on ubuntu machine.
  * you are allowed to remove author credits or you can modify these code accourding to your environment.
  * You are not allowed modify **MiTE AWS PLATFORM** for Reselling Purpose to anyone.
  * feel free and keep your code backup or you can push your branch in same repo.
  * keep in mine while commiting code in our repository. There should not be any credential or confidencial file in your commit.

####  Feel free to Contact me for Any Query:
    Author: Harry
    Skype : HarryTheITExpert
    Email : HarryTheDevOpsGuy@gmail.com
    Blog  : www.Harry-TheDevOpsGuy.blogspot.com

    I will be very happy to resolve your query asap.
