resource "aws_key_pair" "deployer" {
  key_name   = "${var.project}-${lower(var.project_key)}"
  public_key = "${file(var.public_key_file)}"
}
