data "aws_subnet" "default" {
  id                = "${var.default_subnet}"
  availability_zone = "${var.aws_region}b"
  vpc_id            = "${element(data.aws_vpcs.listvpc.ids, 0)}"

  # vpc_id = "${data.aws_vpc.default.id}"
}

# data "aws_vpc" "default" {
#   id = "${var.vpc_id}"
#
#   #  default = true
# }

data "aws_ami" "os" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${lookup(var.ami_os, var.os_name)}"]
  }
}

data "aws_vpcs" "listvpc" {
  # tags {  #   service = "production"  # }
}
