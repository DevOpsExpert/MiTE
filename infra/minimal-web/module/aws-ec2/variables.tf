variable "volume_size" {}
variable "instance_type" {}
variable "aws_ami" {}
variable "environment" {}
variable "common_sg" {}
variable "subnet_id" {}
variable "key_pair" {}
variable "project" {}
variable "owner" {}
variable "creator" {}
variable "requester" {}
variable "project_name" {}
variable "ENVIRONMENT_DESCRIPTION" {}
variable "email" {}
variable "private_key" {}
variable "remote_script" {}

variable "ssh_user" {
  default = "ubuntu"
}
