resource "aws_instance" "miniweb" {
  ami                         = "${var.aws_ami}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_pair}"
  vpc_security_group_ids      = ["${var.common_sg}"]
  subnet_id                   = "${var.subnet_id}"
  associate_public_ip_address = true

  #source_dest_check           = false
  root_block_device = {
    # volume_type          = "gp2"
    volume_size           = "${var.volume_size}"
    delete_on_termination = true
  }

  connection {
    # The default username for our AMI
    user        = "${var.ssh_user}"
    private_key = "${file("${var.private_key}")}"
  }

  provisioner "file" {
    source      = "${var.remote_script}"
    destination = "/tmp/run_remote.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/run_remote.sh",
      "/tmp/run_remote.sh ${lower(var.project)} ${lower(var.environment)}",
    ]
  }

  tags {
    Name                    = "${upper(var.project)}-${upper(var.project_name)}-${upper(var.environment)}"
    Owner                   = "${title(var.owner)}"
    CreatedBy               = "${title(var.creator)}"
    RequestedBy             = "${title(var.requester)}"
    Email                   = "${title(var.email)}"
    Environment             = "${upper(var.project)}-${upper(var.environment)}"
    ENVIRONMENT_DESCRIPTION = "${var.ENVIRONMENT_DESCRIPTION}"
  }
}
