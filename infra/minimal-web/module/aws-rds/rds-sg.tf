# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "rds_sg" {
  name        = "${var.project}-rds-sg"
  description = "Basic Security Group for RDS Server - CreatedBy MITE"

  vpc_id = "${var.vpc_id}"

  #vpc_id = "${data.aws_subnet.default.vpc_id}"

  # SSH access from anywhere
  ingress {
    from_port       = "${var.RDS_DB_PORT}"
    to_port         = "${var.RDS_DB_PORT}"
    protocol        = "tcp"
    cidr_blocks     = "${var.ip_list}"
    security_groups = ["${var.VPC_SECURITY_GROUP}"]
    description     = "${var.managed_by}"
  }

  # ingress {
  #   from_port   = 443
  #   to_port     = 443
  #   protocol    = "tcp"
  #   cidr_blocks = ["0.0.0.0/0"]
  #   description = "${var.managed_by}"
  # }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "${var.managed_by}"
  }
}
