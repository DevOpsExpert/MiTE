resource "aws_iam_user" "s3user" {
  name = "iam-${var.project}-${lower(var.environment)}"
}

resource "aws_iam_access_key" "s3user" {
  user = "${aws_iam_user.s3user.name}"
}

resource "aws_iam_user_policy" "user_policy" {
  name = "${lower(var.project)}-s3-policy"
  user = "${aws_iam_user.s3user.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
                "arn:aws:s3:::${lower(var.project)}-*/*"
            ]
    }
  ]
}
EOF
}
