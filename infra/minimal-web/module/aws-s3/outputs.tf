# output "website_endpoint" {
#   value = "${aws_s3_bucket.s3_bucket.website_endpoint}"
# }
#
# output "website_domain" {
#   value = "${aws_s3_bucket.s3_bucket.website_domain}"
# }

output "SECRET_KEY" {
  value = "${aws_iam_access_key.s3user.secret}"
}

output "ACCESS_KEY" {
  value = "${aws_iam_access_key.s3user.id}"
}

output "BUCKET_NAME" {
  value = "${aws_s3_bucket.s3_bucket.bucket}"
}
